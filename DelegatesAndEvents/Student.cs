namespace DelegatesAndEvents
{
    public class Student
    {
        public string Name { get; set; }
        public int Rating { get; set; }
        
        public override string ToString()
        {
            return $"{Name}, {Rating}";
        }
    }
}