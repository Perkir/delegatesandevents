using System;
using System.Collections.Generic;

namespace DelegatesAndEvents
{
    public static class Extension
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            var max = float.MinValue;
            var maxItem = default(T);
            foreach (var item in e)
            {
                if (!(getParametr(item) >= max)) continue;
                max = getParametr(item);
                maxItem = item;
            }
            return maxItem;
        }
    }
}
