using System;
using System.IO;

namespace DelegatesAndEvents
{
    public class FindFiles
    {
        private string Dir { get; set; }
        public event EventHandler<FileArgs> FileFound;
        public FindFiles(string dir)
        {
            Dir = dir;
        }
        public void Find()
        {
            if (!Directory.Exists(Dir)) return;
            foreach (var filename in Directory.GetFiles(Dir))
            {
                var args = new FileArgs { Name = filename };
                FileFound?.Invoke(this, args);
                if (args.Canceled)
                {
                    break;
                }
            }
        }
    }
}