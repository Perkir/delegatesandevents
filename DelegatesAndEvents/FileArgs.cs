﻿using System;

namespace DelegatesAndEvents
{
    public class FileArgs: EventArgs
    {
        public string Name { get; set; }
        public bool Canceled { get; set; }

    }
}