using System;
using System.Collections.Generic;
using System.IO;

namespace DelegatesAndEvents
{
    public class Program
    {
        static void Main(string[] args)
        { 
            // 1. Написать обобщённую функцию расширения, находящую и возвращающую максимальный элемент коллекции.
            // Функция должна принимать на вход делегат, преобразующий входной тип в число для возможности поиска максимального значения.
            // public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class;
            var student = new List<Student>
            {
                new Student { Name = "Student1", Rating = 5 },
                new Student { Name = "Student2", Rating = 10 },
                new Student { Name = "Student3", Rating = 13 },
                new Student { Name = "Student4", Rating = 17 },
                new Student { Name = "Student5", Rating = 15 },
                new Student { Name = "Student6", Rating = 7 },
                new Student { Name = "Student7", Rating = 20 },
                new Student { Name = "Student8", Rating = 23 },
                new Student { Name = "Student9", Rating = 17 },
                new Student { Name = "Student10", Rating = 19 }
            };
            Console.WriteLine($"Максимальный элемент: {student.GetMax(p => p.Rating)}");

            // 2. Написать класс, обходящий каталог файлов и выдающий событие при нахождении каждого файла
            // 3. Оформить событие и его аргументы с использованием .NET соглашений:
            // public event EventHandler<FileArgs> FileFound;
            // FileArgs – будет содержать имя файла и наследоваться от EventArgs
            // 4. Добавить возможность отмены дальнейшего поиска из обработчика
            // 5. Вывести в консоль сообщения, возникающие при срабатывании событий и результат поиска максимального элемента 
            var curDir = Directory.GetCurrentDirectory();
            var files = new FindFiles(curDir);
            var count = 0;
            Console.WriteLine("Сколько файлов ищем?");
            var fileCount = Convert.ToInt32(Console.ReadLine());
            files.FileFound += (sender, args) =>
            {
                Console.WriteLine($"нашел файл {args.Name}");
                if (++count < fileCount) return;
                args.Canceled = true;
                Console.WriteLine($"Прервано из обработчика, найдено {count} файлов.");
            };
            Console.WriteLine($"Сканируем папку: {curDir}");
            files.Find();
            if (count != fileCount && count < fileCount)
            {
                Console.WriteLine($"Прервано по событию, найдено всего {count} файлов.");
            }
        }
    }
}